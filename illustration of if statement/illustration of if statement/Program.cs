using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace illustration_of_if_statement
{
    class Program
    {
        static void Main(string[] args)
        {
            // This program is to illustrate if else statement in C#
            int iv_1, iv_2;

            while (true)
            {
                Console.WriteLine("Enter a value for the first variable: ");
            iv_1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter a value for the second variable: ");
            iv_2 = int.Parse(Console.ReadLine());

            
            if (iv_1 > iv_2)
                Console.WriteLine($"{iv_1} is higher than {iv_2}");
            else if (iv_2 > iv_1)
                Console.WriteLine($"{iv_2} is higher than {iv_1}");
            else
                Console.WriteLine("Thay are the same");

                Console.WriteLine();
            }



        }
    }
}
