using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Some_output_Options
{
    class Program
    {
        static void Main(string[] args)
        {
            // To show different output options
            Console.WriteLine("Here is 10/3: {0:#.##}", 10.0 / 3.0);
            Console.WriteLine("{0:###,###.###}", 123456.56);
            // C format specifier for monetary values.
            decimal balance;
            balance = 12323.09m;
            Console.WriteLine("Current balance is {0:C}", balance); // uppercase must be used C

            Console.ReadKey();
        }
    }
}
