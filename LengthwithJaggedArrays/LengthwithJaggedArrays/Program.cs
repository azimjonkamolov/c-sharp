using System;

namespace LengthwithJaggedArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] arr= new int[3][];
            arr[0] = new int[] { 0, 1 };
            arr[1] = new int[] { 1, 7 };
            arr[2] = new int[] { 1, 9, 9, 6 };
            int i, i2;

            Console.WriteLine("Display the size of the arrays:");
            Console.WriteLine("The size of the array is " + arr.Length);

            for(i=0;i<arr.Length;i++)
            {
                Console.WriteLine("Length of arr[" + i + "] is " + arr[i].Length);
            }

            Console.WriteLine("Display the content of the arr:");
            for(i=0;i<arr.Length;i++)
            {
                for(i2=0;i2<arr[i].Length;i2++)
                {
                    Console.Write(arr[i][i2] + " ");
                }
                Console.WriteLine();
            }


            Console.Write("Press any key...");
            Console.ReadKey();

        }
    }
}
