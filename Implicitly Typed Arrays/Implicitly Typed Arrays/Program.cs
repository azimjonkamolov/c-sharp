using System;

namespace Implicitly_Typed_Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = new[]
            {
                new[] {1,2,3,4},
                new[] {9,8,7},
                new[] {11,12,13,14,15}
            };

            for(int j=0;j<arr.Length;j++)
            {
                for(int i=0;i<arr[j].Length;i++)
                {
                    Console.Write(arr[j][i] + " ");
                }
                Console.WriteLine();
            }


            Console.Write("Press any key...");
            Console.ReadKey();

        }
    }
}
