// Class with country instance

using System;

class C_Country
{
    static void Main()
    {
        Country USA = new Country();
        Country Korea = new Country();
        Country Australia = new Country();

        USA.name = "The U.S.A";
        USA.num_of_regs = 50;
        USA.capital = "Washington D.C";
        USA.num_of_pop = 330;

        USA.C_Method();
        Console.WriteLine(USA.name + " has " +
            USA.num_of_regs + " states and its capital is " +
            USA.capital + " and number of population is " + USA.num_of_pop + " millions");
        Console.WriteLine();

        
        Korea.name = "The Republic of Korea";
        Korea.capital = "Seoul";
        Korea.num_of_regs = 9;
        Korea.num_of_pop = 52;

        Korea.C_Method();
        Console.WriteLine(Korea.name + " has " +
            Korea.num_of_regs + " provinces and its capital is " +
            Korea.capital + " and number of population is " + Korea.num_of_pop + " millions");
        Console.ReadKey();
    }
}











