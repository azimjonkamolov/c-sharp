﻿// Class with country instance
using System;

class Country
{
    public string name;
    public int num_of_regs;
    public string capital;
    public int num_of_pop;

    public void C_Method()
    {
        Console.WriteLine("The information for " + name);
    }
}