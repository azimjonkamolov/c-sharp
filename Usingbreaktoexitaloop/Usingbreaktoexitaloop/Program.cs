using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usingbreaktoexitaloop
{
    class Program
    {
        static void Main(string[] args)
        {
            // To use a break to exit this loop.
            int i;

            for (i=-10;i<=10;i++)
            {
                if (i > 0) break;
                Console.Write(i + " ");

            }
            Console.WriteLine("Done");
            Console.ReadKey();
            
        }
    }
}
