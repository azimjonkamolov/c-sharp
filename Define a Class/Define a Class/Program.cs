// A program that uses the Building class.
using System;

class Building
{
    public int Floors;
    public int Area;
    public int Occupants;
}

// This class declares an object of type Building
    class BuildingDemo
    {
        static void Main()
        {
        // 

        Building house = new Building(); // to create a Building object
        int areaPP; // area per person

        // Assign values to fields in house.
        house.Occupants = 4;
        house.Area = 2500;
        house.Floors = 2;

        // to compute the area per person.
        areaPP = house.Area / house.Occupants;

        Console.WriteLine("House has:\n" +
            house.Floors + " floors\n" +
            house.Occupants + " occupants\n" +
            house.Area + " total area\n" +
            areaPP + " area per person\n");

        Console.ReadKey(); 
        }
    }
