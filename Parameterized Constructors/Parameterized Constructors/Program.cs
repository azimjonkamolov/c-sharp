// Parameterized Constructors
using System;

class Myclass
{
    public int x;

    public Myclass(int i)
    {
        x = i;
    }
}

class PramConsDemo
{
    static void Main(string[] args)
    {
        Myclass t1 = new Myclass(10);
        Myclass t2 = new Myclass(88);

        Console.WriteLine(t1.x + " " + t2.x);

        Console.ReadKey();

    }
}
