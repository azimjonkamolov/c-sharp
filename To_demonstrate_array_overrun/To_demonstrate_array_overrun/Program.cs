using System;

namespace To_demonstrate_array_overrun
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[10];
            int i, n = 100;

            for(i=0;i<n;i++)
            {
                arr[i] = i; // IndexOutOfRangeExpectation happens here
            }

            Console.Write("Press any key...");
            Console.ReadKey();
        }
    }
}
