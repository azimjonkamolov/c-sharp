// To demonstarte the shift << and >> operators.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitwiseOperators_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int iv_val = 1, i, t;

            for(i = 0; i < 8; i++)
            {
                for(t=128; t>0; t=t/2)
                {
                    if ((iv_val & t) != 0)
                        Console.Write("1 ");
                    else
                        Console.Write("0 ");
                }

                Console.WriteLine();
                iv_val = iv_val << 1; // left shift
            }
            Console.WriteLine();

            iv_val = 128;
            for(i=0;i<8;i++)
            {
                for(t=128;t>0;t=t/2)
                {
                    if ((iv_val & t) != 0)
                        Console.Write("1 ");
                    else
                        Console.Write("0 ");
                }
                Console.WriteLine();
                iv_val = iv_val >> 1; // right shift
            }

            Console.ReadKey();
        }
    }
}
