using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace int_vs_double
{
    class Program
    {
        static void Main(string[] args)
        {
            var ivar = 100;
            var dvar = 100.0;

            Console.WriteLine("ivar contains " + ivar);
            Console.WriteLine("dvar contains " + dvar);

            ivar = ivar / 3;
            dvar = dvar / 3.0;
            Console.WriteLine("");
            Console.WriteLine("ivar after devided by 3 is " + ivar);
            Console.WriteLine("dvar after devided by 3.0 is " + dvar);

            Console.ReadKey();
        }
    }
}
