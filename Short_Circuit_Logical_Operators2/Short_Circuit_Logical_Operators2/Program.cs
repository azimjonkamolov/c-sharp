using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Short_Circuit_Logical_Operators2
{
    class Program
    {
        static void Main(string[] args)
        {
            //
            int i;
            bool someCondition = false;
            i = 0;

            // Here, i is still incremented even
            // though the if statement fails.
            if (someCondition & (++i < 100))
                Console.WriteLine("This won't be displayed");

            Console.WriteLine("if statement is executed " + i); // still 1 !!
            Console.ReadKey();

        }
    }
}
