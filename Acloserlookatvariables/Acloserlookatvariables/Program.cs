using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acloserlookatvariables
{
    class Program
    {
        static void Main(string[] args)
        {
            // Length of sides
            double s1 = 4.0;
            double s2 = 5.0;

            // Dynamically initialize hypot
            double hypot = Math.Sqrt((s1 * s1) + (s2 * s2));

            Console.Write("Hypotenuse of triangle with sides " + s1 + " by " + s2 + " is ");

            Console.WriteLine("{0:#.###}.", hypot);

            Console.ReadKey();
        }
    }
}
