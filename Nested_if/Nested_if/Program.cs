using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nested_if
{
    class Program
    {
        static void Main(string[] args)
        {
            // How to use a nested loop
            int i;

            for (i = -5; i<= 5;i++)
            {
                Console.Write("Testing " + i + ": ");

                if (i < 0)
                    Console.WriteLine("negative");
                else if (i == 0)
                    Console.WriteLine("no sign");
                else
                    Console.WriteLine("positive");
            }

            Console.ReadKey();
        }
    }
}


