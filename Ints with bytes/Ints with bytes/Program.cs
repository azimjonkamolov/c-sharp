using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ints_with_bytes
{
    class Program
    {
        static void Main(string[] args)
        {
            // To use byte and ints
            byte x;
            int sum;

            sum = 0;
            for (x = 1; x <= 100; x++)
                sum = sum + x;

            Console.WriteLine("Summation of 100 is " + sum);

            Console.ReadKey();
        }
    }
}
