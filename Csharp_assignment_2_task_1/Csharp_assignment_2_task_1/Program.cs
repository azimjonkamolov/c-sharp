// Author: Azimjon Kamolov
// Email: azimjon.6561@gmail.com
// Purpose: To check wether a given number is odd or even

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csharp_assignment_2_task_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            char y;
            while(true) // an infinite loop to use more times
            {
                Console.Write("Enter a number: ");
                n = int.Parse(Console.ReadLine());
                if (n % 2 == 0)
                    Console.WriteLine(n + " is an even number");
                else
                    Console.WriteLine(n + " is an odd number");
                Console.Write("To continue, hit y: ");
                y = char.Parse(Console.ReadLine());
                if (y != 'y') // if the input is not 'y', then terminates
                    break;

                Console.WriteLine();
            }

        }
    }
}
