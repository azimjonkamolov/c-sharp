/*
Author: Azimjon Kamolov
Purpose: Assignment 1 >> task 2
Date: 03.21.2019
Email: azimjon.6561@gmail.com
Note: Please read the README.txt file
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            byte a = 100; // To store 100 to a
            byte b = 200; // To store 200 to b
            byte c = (byte)(a+b); // To store the sum of a and b to c (byte data type) 
            int c1 = a + b; // To store the sum of a and b ot c1 (int data type)
            Console.WriteLine("a = " + a);
            Console.WriteLine("b = " + b);
            Console.WriteLine("The sum of 100 + 200 when stored with byte data type: " + c); // here the output is a garbage value as byte can only store up to 250
            Console.WriteLine("The sum of 100 + 200 when stored with int data type: " + c1); // here the output is a right answer as int can store a larger number compared to byte

            Console.ReadKey(); // To hold the screen
        }
    }
}
