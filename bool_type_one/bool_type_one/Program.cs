using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bool_type_one
{
    class Program
    {
        static void Main(string[] args)
        {
            // To work with bool data type
            bool b;

            b = false;
            Console.WriteLine("This cannot be seen as b is  " + b);
            b = true;
            Console.WriteLine("This can be seen as b is " + b);
            if (b) // can be used with if
                Console.WriteLine("This if statement can be seen");
            b = false;
            if (b)
                Console.WriteLine("This if cannot be seen");
            Console.WriteLine("10<9 is " + (10 < 9));

            Console.ReadKey();
        }
    }
}
