// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Text;
// using System.Threading.Tasks;

namespace without_using_System
{
    class Program
    {
        static void Main(string[] args)
        {
            // This program is to use without using System
            // just like C++ where using namespace std;

            System.Console.WriteLine("Info pops up!");
            System.Console.ReadKey();

        }
    }
}
