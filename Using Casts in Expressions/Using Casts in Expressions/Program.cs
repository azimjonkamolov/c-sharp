using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Using_Casts_in_Expressions
{
    class Program
    {
        static void Main(string[] args)
        {
            // The following program displays the square roots of the numbers from 1 to 10
            double n;
            for(n=1.0;n<=10;n++)
            {
                Console.WriteLine("The square root of {0} is {1} ", n, Math.Sqrt(n));
                Console.WriteLine("Whole number part: {0}", (int) Math.Sqrt(n));
                Console.WriteLine("Fractional part: {0}", Math.Sqrt(n), (int)Math.Sqrt(n));
                Console.WriteLine();
            }

            Console.WriteLine("\nDone!");
            Console.ReadKey();

          
        }
    }
}
