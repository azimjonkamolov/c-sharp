using System;

namespace Using_Strings_in_switch_statements
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] sarr = { "one", "two", "three", "two", "one" };

            foreach(string s in sarr)
            {
                switch(s)
                {
                    case "one":
                        Console.Write("1");
                        break;
                    case "two":
                        Console.Write("2");
                        break;
                    case "three":
                        Console.Write("3");
                        break;
                }
            }

            Console.WriteLine();
            Console.ReadKey();


        }
    }
}
