using System;

class Building
{
    public int floors;
    public int area;
    public int occupants;

    // to display the area per person
    public void AreaPerPerson()
    {
        Console.WriteLine(area / occupants + " area per person");
    }
}

    class BuildingDemo
    {
        static void Main(string[] args)
        {
        // 
        Building house = new Building();
        Building office = new Building();

        // Assign values to fields in house
        house.floors = 4;
        house.area = 2500;
        house.occupants = 2;

        // Assign values to fields in office
        office.floors = 4;
        office.area = 2500;
        office.occupants = 2;

        Console.WriteLine("house has:\n" +
            house.floors + " floors\n" +
            house.occupants + " occupants\n" +
            house.area + " total area");
        house.AreaPerPerson();

        Console.WriteLine();

        Console.WriteLine("office has:\n" +
            office.floors + " floors\n" +
            office.occupants + " occupants\n" +
            office.area + " total area");
        office.AreaPerPerson();

        Console.ReadKey();

        }
    }
