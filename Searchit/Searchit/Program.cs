using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchit
{
    class Program
    {
        static void Main(string[] args)
        {
            // To find a word if used there
            var sv_words = "You say the greatest, and I am there!";
            Console.WriteLine(sv_words.Contains("am")); // if am is there then will be printed out
            Console.WriteLine(sv_words.Contains("tom")); // if tom is there then will be printed out
            //Note: when I wrote here it said true as there contains here :)

            Console.WriteLine(sv_words.StartsWith("You"));
            Console.WriteLine(sv_words.EndsWith("go"));


            Console.ReadLine();
        }
    }
}
