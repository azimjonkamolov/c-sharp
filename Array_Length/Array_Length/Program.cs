using System;

namespace Array_Length
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] num = new int[10];
            int i, n = num.Length;

            for(i=0;i<n;i++)
            {
                num[i] = i * i;
            }

            for(i=0;i<n;i++)
            {
                Console.Write(num[i] + " ");
            }

            Console.WriteLine();
            Console.Write("Press any key...");
            Console.ReadKey();

        }
    }
}
