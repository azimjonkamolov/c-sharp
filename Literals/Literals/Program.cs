using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Literals
{
    class Program
    {
        static void Main(string[] args)
        {
            // To demonstrate escape sequences in strings.

            Console.WriteLine("Line One\nLine Two\nLine Three");
            Console.WriteLine("One\tTwo\tThree");
            Console.WriteLine("Four\tFive\tSix");

            // Embed quotes.
            Console.WriteLine("\"Why?\", he asked.");

            Console.ReadKey();

        }
    }
}
