using System;


namespace nums_in_words
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            int nextdigit=0;
            int numdigits=0;
            int[] iarr = new int[20];

            string[] sarr = {
                "zero", "one", "two", "three",
                "four", "five", "six", "seven",
                "eight", "nine"
                          };

            Console.Write("Enter the number to be displayed: ");
            num=int.Parse(Console.ReadLine());

            // Get inividual digits and store in n
            // These digits are stored in reverse order.
            do
            {
                nextdigit = num % 10;
                iarr[numdigits] = nextdigit;
                numdigits++;
                num = num / 10;
            } while (num > 0);
            numdigits--;

            // Display the words.
            for (; numdigits >= 0; numdigits--)
                Console.Write(sarr[iarr[numdigits]] + " ");

            Console.WriteLine();
            Console.Write("Press any key...");
            Console.ReadKey();

        }
    }
}
