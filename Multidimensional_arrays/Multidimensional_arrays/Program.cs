using System;

namespace Multidimensional_arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,,] arr = new int[3, 3, 3];
            int sum = 0, n = 1, x, y, z;

            for(x=0;x<3;x++)
            {
                for(y=0;y<3;y++)
                {
                    for(z=0;z<3;z++)
                    {
                        Console.WriteLine("N = " + n);
                        arr[x, y, z] = n++;
                        sum = arr[0, 0, 0] + arr[1, 1, 1] + arr[2, 2, 2];
                    }
                }
            }
            Console.WriteLine("The sum of first dimention is " + sum);

            Console.WriteLine("Press any key...");
            Console.ReadKey();

        }
    }
}
