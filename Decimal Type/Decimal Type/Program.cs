using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decimal_Type
{
    class Program
    {
        static void Main(string[] args)
        {
            // How to use decimal type
            // Decimal contains m with numbers

            decimal price;
            decimal discount;
            decimal discounted_price;

            // Compute discounted price
            price = 19.95m;
            discount = 0.15m;
            // discount rate is 15%

            discounted_price = price - (price * discount);
            Console.WriteLine("Discounted price: $" + discounted_price);

            Console.ReadKey();

        }
    }
}
