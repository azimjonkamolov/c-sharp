using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Using_Code_Blocks
{
    class Program
    {
        static void Main(string[] args)
        {
            // To have product and sum using loop statement
            int i, iv_pro=1, iv_sum=0, n;

            Console.WriteLine("Determine up to what number must be calculated: ");
            n = int.Parse(Console.ReadLine());

            for (i=1; i<=n; i++)
            {
                iv_pro = iv_pro * i;
                iv_sum = iv_sum + i;
            }

            Console.WriteLine("The sum: " + iv_sum);
            Console.WriteLine("The product: " + iv_pro);

            Console.ReadKey();

            
        }
    }
}
