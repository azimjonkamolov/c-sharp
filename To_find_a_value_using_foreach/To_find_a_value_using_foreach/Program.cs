using System;

class Find_a_Value
{
    static void Main()
    {
        int[,] arr = new int[5,5];
        int i, j, n=1, size, value, flag=0, loc1=0, loc2=0;
        size = arr.Length;

        for(i=0; i<5; i++)
        {
            for(j=0; j<5; j++)
            {
                arr[i, j] = n++;
            }
        }

        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 5; j++)
            {
                Console.Write(arr[i, j] + " ");
            }
            Console.WriteLine();
        }

        Console.Write("Enter the value to search: ");
        value = int.Parse(Console.ReadLine());

        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 5; j++)
            {
                if(value == arr[i,j])
                {
                    loc1 = i;
                    loc2 = j;
                    flag = 1;
                }
            }
        }

        if(flag == 1)
        {
            Console.WriteLine("The value is found in the location of " + loc1 + " " + loc2);
        }
        else
        {
            Console.WriteLine("The value could not be found :(");
        }

        Console.WriteLine();
        Console.Write("Press any key...");
        Console.ReadKey();
    }
}