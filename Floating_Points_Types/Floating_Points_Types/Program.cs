using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Floating_Points_Types
{
    class Program
    {
        static void Main(string[] args)
        {
            // To show how to use floating point data types
            Double r;
            Double area;

            area = 10.0;

            r = Math.Sqrt(area / 3.1416);

            Console.WriteLine("Radius is " + r);
            Console.ReadKey();
        }
    }
}
