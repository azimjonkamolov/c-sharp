// Using new with Value Type
using System;



class newValue
{
    static void Main(string[] args)
    {
        // initialize it to zero
        int i = new int();

        Console.WriteLine("The value of i is: " + i); // without new, it causes the error

        Console.ReadKey();

    }
}
