using System;

namespace Lessthanactuallength
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[10];
            int i, n = arr.Length;

            arr[0] = 1;
            arr[1] = 2;
            arr[2] = 3;
            arr[3] = 4;
            arr[4] = 5;

            for (i = 0; i < n; i++)
                Console.Write(arr[i] + " "); // gives zero as an unindicated numbers after 5

            Console.WriteLine();
            Console.Write("Press any key...");
            Console.ReadKey();

        }
    }
}
