using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Using_continue
{
    class Program
    {
        static void Main(string[] args)
        {
            // To use continue satement
            // Print even numbers between 0 and 100.
            int i;

            for (i = 0; i<=100; i++)
            {
                if ((i % 2) != 0) continue; // iterate if an odd number
                Console.Write(i + " "); 
            }
            Console.ReadKey();

             
        }
    }
}
