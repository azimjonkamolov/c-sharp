/*
Author: Azimjon Kamolov
Purpose: Assignment 1 >> task 1
Date: 03.21.2019
Email: azimjon.6561@gmail.com
Note: Please read the README.txt file
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i, j, n, iv_rows;
            int iv_stars, iv_spaces;

            Console.WriteLine("Enter num of rows to print out: ");
            iv_rows=int.Parse(Console.ReadLine());

            iv_stars = 1;
            iv_spaces = iv_rows - 1;

            /* Iterate through rows */
            for (i = 1; i < iv_rows * 2; i++)
            {
                
                for (j = 1; j <= iv_spaces; j++)    // This loop is to create space
                    Console.Write(" ");

                /* Print stars */
                for (j = 1; j < iv_stars * 2; j++)  // This loop is to create stars
                    Console.Write("*");

                Console.Write("\n");    // To create a new line

                if (i < iv_rows)
                {
                    iv_spaces--;
                    iv_stars++;
                }
                else
                {
                    iv_spaces++;
                    iv_stars--;
                }
            }
            Console.ReadKey(); // To hold the screen
        }
    }
}
