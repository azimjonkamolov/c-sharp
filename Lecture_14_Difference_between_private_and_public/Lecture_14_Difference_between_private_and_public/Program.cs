using System;

class Myclass
{
    private int x1; // declared as a private data type
    int x2; // private data type by default
    public int y; // declared as a public data type

    public void Setx1(int a)
    {
        x1 = a;
    }

    public int Getx1()
    {
        return x1;
    }

    public void Setx2(int b)
    {
        x2 = b;
    }

    public int Getx2()
    {
        return x2;
    }
}

class AccessMyclass
{
    static void Main()
    {
        Myclass ob = new Myclass();
        int a, b;
        Console.Write("Enter the first class value here: ");
        a = int.Parse(Console.ReadLine());
        Console.Write("Enter the second class value here: ");
        b = int.Parse(Console.ReadLine());

        ob.Setx1(a);
        ob.Setx2(b);
        ob.y = 1996;

        Console.WriteLine("My birthday is " + ob.Getx1() + "." + ob.Getx2() + "." + ob.y);
        Console.ReadKey();
    }
}