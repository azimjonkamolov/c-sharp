using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace How_to_use_reserved_words
{
    class Program
    {
        static void Main(string[] args)
        {
            // To use reserved words
            int @int=1, @for=2, @if=3;

            Console.WriteLine(@int + " " + @for + " " + @if);

            Console.ReadKey();

        }
    }
}
