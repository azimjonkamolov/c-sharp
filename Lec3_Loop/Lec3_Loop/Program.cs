using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lec3_Loop
{
    class Program
    {
        static void Main(string[] args)
        {
            // To use Loop
            int iv_1, iv_upto, iv_sum=0;
            iv_upto = 5;
            int @if=0, @for=7;

            for (iv_1 = 0; iv_1 < iv_upto; iv_1++)
                Console.WriteLine(iv_1);

            Console.WriteLine("The first one is done!");

            for (iv_1 = 0; iv_1 < iv_upto; iv_1++)
            {
                iv_sum += iv_1;
            }

            Console.WriteLine("\n");

            Console.WriteLine($"The sum of the loop: {iv_sum}");

            Console.WriteLine("\n");
            for (@if = 0; @if < @for; @if++)
            {
                Console.WriteLine($"The num is {@if}");
            }




            Console.ReadLine();
        }
    }
}
