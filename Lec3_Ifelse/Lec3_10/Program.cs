using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lec3_10
{
    class Program
    {
        static void Main(string[] args)
        {
            // To use if and else
            int iv_1, iv_2, iv_3;
            iv_1 = 2;
            iv_2 = 3;

            if (iv_1 > iv_2)
                Console.WriteLine($"{iv_1} is greater than {iv_2}");
            if (iv_1 == iv_2)
                Console.WriteLine($"{iv_1} is equal to {iv_2}");
            if (iv_1 < iv_2)
                Console.WriteLine($"{iv_2} is greater than {iv_1}");

            Console.ReadLine();
        }
    }
}
