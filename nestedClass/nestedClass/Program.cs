using System;

class Time
{
    public int age;
    public int time;
}

class Name
{
    public Time name = new Time();
    public Time year = new Time(); 

}

class Student
{
    public static void Main()
    {
        Name myname = new Name();
        myname.name.age = 24;
        Console.WriteLine("The age of the first person is " + myname.name.age);

        Console.ReadKey();
    }
}