using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheConditionalOperator2
{
    class Program
    {
        static void Main(string[] args)
        {
            for(int i = -5; i < 6; i++)
            {
                if (i != 0 ? (i % 2 == 0) : false)
                    Console.WriteLine("100 / " + i + " is " + 100 / i);
            }

            Console.ReadKey();
        }
    }
}
