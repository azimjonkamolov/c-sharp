using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ternary_operator
{
    class Program
    {
        static void Main(string[] args)
        {
            // How to use a ternary operator
            int abs, val;
            Console.WriteLine("Enter a number: ");
            val = int.Parse(Console.ReadLine());

            abs = val < 0 ? val : val = val - val * 2; // get absolute value of val

            Console.WriteLine("The absolute val of the number entered is " + abs);
            Console.ReadLine();

        }
    }
}
