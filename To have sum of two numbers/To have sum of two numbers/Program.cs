// Author: Azimjon Kamolov
// Email: azimjon.6561@gmail.com

using System; // Only this "using" is used in this code
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace To_have_sum_of_two_numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            // This is to have the sum of two numbers using int data type
            int iv_1, iv_2, iv_sum = 0;
            Console.WriteLine("Enter the first number: ");
            iv_1 = int.Parse(Console.ReadLine()); // To insert a value inside of iv_1
            Console.WriteLine("Enter the second number: ");
            iv_2 = int.Parse(Console.ReadLine()); // To insert a value inside of iv_2
            iv_sum = iv_1 + iv_2; // To calculate a sum of the entered numbers
            Console.WriteLine($"The sum of {iv_1} and {iv_2} is {iv_sum}!");

            Console.ReadKey(); // To hold a screen 
        }
    }
}
