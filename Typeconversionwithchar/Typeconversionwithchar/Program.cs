using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Typeconversionwithchar
{
    class Program
    {
        static void Main(string[] args)
        {
            // To see with char data type
            char ch1 = 'a', ch2 = 'b';
            ch1 = (char)(ch1 + ch2);

            Console.WriteLine("ch1: " + ch1);
            Console.WriteLine("ch1 as int: " + (int)ch1);
            Console.ReadKey();

        }
    }
}
