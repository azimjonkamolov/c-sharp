using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Two_dimensional_arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] arr = new int[3, 4];
            int i, t, num=1;
            for(t=0;t<3;t++)
            {
                for(i=0;i<4;i++)
                {
                    arr[t, i] = num ++;
                    Console.Write(arr[t, i] + " ");
                    if (i == 3)
                        Console.WriteLine();
                }
            }
            Console.Write("Press any key...");
            Console.ReadKey();

        }
    }
}
