// Author: Azimojon Kamolov
// Purpose: Bank account

using System;

class Bank
{
    public string a_name;
    public int a_number;
    public string a_type;
    public int balance;

    public void Value()
    {
        balance = 0;
    }

    public void Withdrow(int withdrow) // withdrow function
    {

        if(balance>withdrow)
        {
            balance = balance - withdrow;
            Console.WriteLine("The amount of money in your bank account is " + balance);
        }
        else
        {
            Console.WriteLine("Not enough amount of money in the account!");
        }
    }

    public void Deposit(int deposit) // deposit function
    {
        balance += deposit;
        Console.WriteLine("The amount of money in your bank account is " + balance);


    }
}

class User
{
    static void Main()
    {
        Bank user = new Bank();
        int choice, withdrow, deposit;

        Console.WriteLine("\t\tWelcome!");
        Console.Write("Enter the depositor's name: ");
        user.a_name = Console.ReadLine();
        Console.WriteLine("The account owner's name is " + user.a_name);

        Console.Write("Enter the depositor's account number: ");
        user.a_number =int.Parse(Console.ReadLine());
        Console.WriteLine("The account's number is " + user.a_number);

        Console.Write("Enter your account type: ");
        user.a_type = Console.ReadLine();
        Console.WriteLine("Yuor account type is " + user.a_type);

        user.Value();
        Console.WriteLine("The amount of money you have is " + user.balance);

        while(true)
        {
            Console.WriteLine();
            Console.WriteLine("Choose one of the actions bellow:");
            Console.WriteLine("1. Deposit");
            Console.WriteLine("2. Withdrow");
            Console.WriteLine("3. Balance");
            Console.WriteLine("4. Exit");
            choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.Write("Enter the amount to deposit: ");
                    deposit = int.Parse(Console.ReadLine());
                    user.Deposit(deposit);
                    break;
                case 2:
                    Console.Write("Enter the amount to be withdrown: ");
                    withdrow = int.Parse(Console.ReadLine());
                    user.Withdrow(withdrow);
                    break;
                case 3:
                    Console.WriteLine(user.a_name);
                    Console.WriteLine("The amount of money in your bank account is " + user.balance);
                    break;
                case 4:
                    System.Environment.Exit(1);
                    break;
                default:
                    Console.WriteLine("Unknown action!");
                    break;
                
            }
        }
    }
}