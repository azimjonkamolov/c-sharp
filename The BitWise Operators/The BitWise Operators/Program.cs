using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_BitWise_Operators
{
    class Program
    {
        static void Main(string[] args)
        {
            // 
            ushort num;
            ushort i;
            for (i = 1; i <= 10; i++)
            {
                num = i;
                Console.WriteLine("num: " + num);
                num = (ushort)(num & 0xFFFE); // num & 1111 1110
                Console.WriteLine("num after turning off bit zero: " + num + "\n");
            }

            Console.ReadKey();
        }
    }
}
