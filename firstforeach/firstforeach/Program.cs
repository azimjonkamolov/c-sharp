using System;

class Foreach
{
    static void Main()
    {
        int[] arr = new int[10];
        int i, sum = 0, n;
        n = arr.Length;


        for(i=0;i<n;i++)
        {
            arr[i] = i;
        }

        foreach(var j in arr) // var can also be used 
        {
            Console.WriteLine("Value is: " + arr[j]);
            sum += j;
        }

        Console.WriteLine("Summation: " + sum);

        Console.Write("Press any key...");
        Console.ReadKey();
    }
}