/* For example, the C# syntax permits the name of parameter or a local variable
 * to be tha same as the name of an instance variable
 * >> when this happens, the local name hides the instance variable
 */

using System;

class Rect
{
    int width;
    int height;

    public Rect(int width, int height)
    {
        this.height = height;
        this.width = width;
    }
}

class RectDemo
{
    static void Main()
    {
        Rect num1 = new Rect(10, 20);

        Console.WriteLine("The height is " + num1);

        Console.WriteLine("The height is " + num1);

        Console.ReadKey();

    }
}