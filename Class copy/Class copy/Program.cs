// Variables with same data
using System;

class Name
{
    public string name;
    public int age;
}

class Person
{
    static void Main()
        {
        Name person1 = new Name();
        Name person2 = person1;

        person1.name = "Tom";
        person1.age = 20;

        Console.WriteLine("The name of the first person " + person1.name);
        Console.WriteLine("The age of the first person " + person1.age);
        Console.WriteLine();
        Console.WriteLine("The name of the second person " + person2.name);
        Console.WriteLine("The age of the second person " + person2.age);

        Console.ReadKey();
        
        }
}