using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Assignment_Operator
{
    class Program
    {
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            int x, y, z;
            x = y = z = 100; // This action is allowed in C#, Python, Java, but not in C++, and C
            Console.WriteLine("x = " + x);
            Console.WriteLine("y = " + y);
            Console.WriteLine("z = " + z);




            Console.ReadKey();
        }
    }
}
