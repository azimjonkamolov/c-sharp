using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thedowhileloop
{
    class Program
    {
        static void Main(string[] args)
        {
            // T
            int num;
            int nextdigit;
            num = 198;
            Console.WriteLine("Number: " + num);
            Console.Write("Number in reverse order: ");
            do
            {
                nextdigit = num % 10;
                Console.Write(nextdigit);
                num = num / 10;

            } while (num > 0);

            Console.WriteLine();
            Console.ReadKey();
        }

    }
}
