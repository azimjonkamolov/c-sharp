using System;

class Building
{
    public int Floors;
    public int Area;
    public int Occupants;

    public Building(int a, int b, int c)
    {
        Floors = a;
        Area = b;
        Occupants = c;
    }

    public int NumofFloors()
    {
        return Floors;
    }
}

class BuildingDemo
{
    static void Main()
    {
        Building house = new Building(2, 3, 4);

        Console.WriteLine("The number of floors in the house are " + house.Floors);

        Console.ReadKey();
    }
}











