using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shift_Operators
{
    class Program
    {
        static void Main(string[] args)
        {
            // To show how to use Shift Operator
            int iv_1, iv_2;
            Console.Write("Enter int number to be multiplied by 2 using shift operator (<<): ");
            iv_1 = int.Parse(Console.ReadLine());
            iv_2 = iv_1;
            iv_2 = iv_2 << 1; // Moving one bit bits increases amount two times <<
            Console.WriteLine(iv_1 + " multiplid by 2 is " + iv_2);

            Console.WriteLine();

            Console.Write("Enter int number to be devided by 2 using shift operator (>>): ");
            iv_1 = int.Parse(Console.ReadLine());
            iv_2 = iv_1;
            iv_2 = iv_2 >> 1; // Moving one bit bits decreases amount two times >>
            Console.WriteLine(iv_1 + " devided by 2 is " + iv_2);

            Console.ReadKey();
        }
    }
}
