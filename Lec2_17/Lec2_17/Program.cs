using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lec2_17
{
    class Program
    {
        static void Main(string[] args)
        {
            // To calculate two numbers
            int iv_1, iv_2, iv_sum = 0;
            Console.WriteLine("Enter the two numbers:");
            iv_1 = int.Parse(Console.ReadLine());
            iv_2 = int.Parse(Console.ReadLine());

            iv_sum = iv_1 + iv_2;

            Console.WriteLine($"The sum of {iv_1} and {iv_2} is {iv_sum}!");
            Console.ReadLine();

        }
    }
}
