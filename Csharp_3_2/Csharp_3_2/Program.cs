// Author: Azimjon Kamolov
// Gmail: azimjon.6561@gmail.com
// Purpose: C# assignment 3 task 2

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csharp_3_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            while(true)
            {
                Console.WriteLine("Enter country code: ");
            name = Console.ReadLine();
            name.ToLower();
            switch(name)
            {
                case "us":
                    Console.WriteLine("United States");
                    break;
                case "uz":
                    Console.WriteLine("Uzbekistan");
                    break;
                case "kr":
                    Console.WriteLine("Korea");
                    break;
                case "de":
                    Console.WriteLine("Germany");
                    break;
                case "uk":
                    Console.WriteLine("United Kingdom");
                    break;
                default:
                    Console.WriteLine("Unknown");
                    break;


            }
                Console.WriteLine();
            }
        }
    }
}
