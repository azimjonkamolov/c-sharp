using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Type_Conversion_in_Expressions
{
    class Program
    {
        static void Main(string[] args)
        {
            // To see a promotion surprise !
            byte b;
            b = 10;
            b = (byte)(b * b); // cast needed! >> as b is accepted as a small int

            Console.WriteLine("b: " + b);
            Console.ReadKey();
        }
    }
}
