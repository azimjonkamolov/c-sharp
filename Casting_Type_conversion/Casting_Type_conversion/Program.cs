using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting_Type_conversion
{
    class Program
    {
        static void Main(string[] args)
        {

            // To declare data types
            int iv_num = 100;
            string sv_name = "";
            float fv_float = 15.5f;

            // To cast data types
            sv_name = iv_num.ToString();
            sv_name = fv_float.ToString();
            fv_float = iv_num;
            iv_num = (int)14.7;

            // To give the outputs
            Console.WriteLine(sv_name);
            Console.WriteLine(fv_float);
            Console.WriteLine(iv_num);

            Console.Read();
        }
    }
}
