// Author: Azimjon Kamolov
// Email: azimjon.6561@gmail.com

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Future_Value_of_Investment
{
    class Program
    {
        static void Main(string[] args)
        {
            // To calculate the future value of investment
            decimal amount;
            decimal rate_of_return;
            int years, i;

            // If decimal values are inserted, m is not required the system knows automatically
            Console.Write("Enter the amount of original investment: ");
            amount = decimal.Parse(Console.ReadLine());
            Console.Write("Rate of return: ");
            rate_of_return = decimal.Parse(Console.ReadLine());
            Console.Write("For how many years: ");
            years = int.Parse(Console.ReadLine());

            for (i = 0; i < years; i++)
                amount = amount + (amount * rate_of_return);

            Console.WriteLine("Future value is " + amount);
            Console.ReadKey();

        }
    }
}
