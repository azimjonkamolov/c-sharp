using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lec4_integers
{
    class Program
    {
        static void Main(string[] args)
        {
            // Integers
            long lv_inches;
            long lv_miles;

            lv_miles = 93000000;
            // 93,000,000 miles to the sun
            // 5,280 feet in a mile,
            // 12 inches in a foot.
            lv_inches = lv_miles * 5280 * 12;

            Console.WriteLine("Distance to the sun: " + lv_inches + " inches.");

            Console.ReadLine();
            

        }
    }
}
