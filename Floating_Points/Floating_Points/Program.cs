using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Floating_Points
{
    class Program
    {
        static void Main(string[] args)
        {
            // More floating points
            Double r;
            Double area;

            area = 10.0;

            r = Math.Sqrt(area / 3.1416);

            Console.WriteLine("Radius is " + r);
            Console.ReadKey();
        }
    }
}
