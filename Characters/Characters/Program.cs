using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Characters
{
    class Program
    {
        static void Main(string[] args)
        {
            // To show how char data types work
            char ch;
            ch = 'X';
            // ch = 88 is not allowed only allowed by type casting
            Console.WriteLine("This is ch: " + ch);
            Console.ReadKey();

        }
    }
}
