using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Output_Options
{
    class Program
    {
        static void Main(string[] args)
        {
            // To show and deside the size of nums
            // {1,4} 1 = to show location and 4 is to show the size 
            Console.WriteLine("February has {0} or {1} days.", 28, 29);
            Console.WriteLine("February has {0,10} or {1,5} days.", 28, 29); // {0,10} = %10d in C
            Console.ReadLine();
        }
    }
}
