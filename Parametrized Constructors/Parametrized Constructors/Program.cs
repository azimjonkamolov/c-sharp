using System;

class MyClass
{
    public int x;

    public MyClass(int i) // must be with the same name
    {
        x = i;
    }
}

class Parm
{
    static void Main()
    {
        MyClass t1 = new MyClass(10);
        MyClass t2 = new MyClass(20);

        Console.WriteLine(t1.x + " " + t2.x);

        Console.ReadKey();
    }
}