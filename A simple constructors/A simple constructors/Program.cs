// A simple constructos
using System;

class Myclass
{
    public int x;

    public Myclass()
    {
        x = 10;
    }
}

class Program
{
    static void Main(string[] args)
    {
        //

        Myclass t1 = new Myclass();
        Myclass t2 = new Myclass();

        Console.WriteLine(t1.x + " " + t2.x);

        Console.ReadKey();
    }
}
