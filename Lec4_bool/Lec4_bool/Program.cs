using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lec4_bool
{
    class Program
    {
        static void Main(string[] args)
        {
            // 
            bool bv_info;
            bv_info = false;
            Console.WriteLine("b is " + bv_info);
            bv_info = true;
            Console.WriteLine("b is " + bv_info);

            // A bool value can control
            // the if statement.
            if (bv_info)
                Console.WriteLine("This is executed :)");

            bv_info = false;
            if (bv_info)
                Console.WriteLine("This is not executed :(");

            Console.WriteLine("10 > 9 is " + (10 > 9));
            Console.ReadKey();
            

        }
    }
}
