using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace upper_lower
{
    class Program
    {
        static void Main(string[] args)
        {
            // To turn lower and upper case letters
            var sv_info = "Hello Amigos!";

            Console.WriteLine($"As a uppercase: {sv_info.ToUpper()}");
            Console.WriteLine($"As a lowercase: {sv_info.ToLower()}");

            Console.ReadLine();
        }
    }
}
