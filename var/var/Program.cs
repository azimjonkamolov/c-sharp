using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace var
{
    class Program
    {
        static void Main(string[] args)
        {
            // variables with defined data types
            int iv_num = 17;
            string sv_name = "Azim";

            // variables without data types
            var v_var = "Tom";
            var v_var1 = 15;
            var v_text = "This is a text";

            // To gain outputs
            Console.WriteLine("iv_num: " + iv_num);
            Console.WriteLine("sv_name: " + sv_name);
            Console.WriteLine("v_var: " + v_var);
            Console.WriteLine("v_var1: " + v_var1);
            Console.WriteLine("v_text: " + v_text);

            Console.Read();

        }
    }
}
