using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globalization
{
    class Program
    {
        static void Main(string[] args)
        {
            // without << using System.Globalization >> we cannot use this functions
            decimal value = 123.00M;
            Console.WriteLine(value.ToString("C", CultureInfo.CurrentCulture)); // as my computer was said that the location is in US, it showed the US dollor sign
            // Change currency symbol
            Console.WriteLine(value.ToString("C", CultureInfo.CreateSpecificCulture("en-US")));
            Console.WriteLine(value.ToString("C", CultureInfo.CreateSpecificCulture("fr-BE")));
            Console.WriteLine(value.ToString("C", CultureInfo.CreateSpecificCulture("en-GB")));
            Console.WriteLine(value.ToString("C", CultureInfo.CreateSpecificCulture("ja-JP")));

            Console.ReadKey();
        }
    }
}
