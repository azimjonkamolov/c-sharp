using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheWhileLoop1
{
    class Program
    {
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            int e, result, i;
            
            for(i = 0; i<10;i++)
            {
                result = 1;
                e = i;
                while(e>0)
                {
                    result *= 2;
                    e--;
                }
                Console.WriteLine("2 to the " + i + " power is " + result);
            }
            
            Console.ReadKey();
        }
    }
}
