using System;

class String
{
    static void Main()
    {
        char[] charr = { 't', 'e', 's', 't' };
        string str = new string(charr);
        string str1 = "Hello you";

        Console.WriteLine(str);
        Console.WriteLine(str1);

        Console.Write("Press any key...");
        Console.ReadKey();
    }
}