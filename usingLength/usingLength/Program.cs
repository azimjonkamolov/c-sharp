using System;


namespace usingLength
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[10];
            int[] arr1 = new int[10];
            int i, j, n = arr.Length;

            Console.Write("The original: ");
            for (i = 0; i < n; i++)
            {
                arr[i] = i;
                Console.Write(arr[i] + " ");
            }

            Console.WriteLine();

            Console.Write("The reverse: ");
            for (i=0, j=n-1;i<n; i++, j--)
            {
                arr1[j] = arr[i];
            }

            for (j = 0; j < n; j++)
            {
                arr[j] = j;
                Console.Write(arr1[j] + " ");
            }

            Console.WriteLine();
            Console.Write("Press any key...");
            Console.ReadKey();

        }
    }
}
