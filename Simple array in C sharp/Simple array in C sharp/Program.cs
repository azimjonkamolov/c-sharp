using System;

namespace Simple_array_in_C_sharp
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] sample = new int[10];
            int i;

            for(i=0;i<10;i++)
            {
                sample[i] = i;

                Console.WriteLine("sample[" + sample[i] + "]");

            }

            Console.Write("Press any key...");
            Console.ReadKey(); 
        }
    }
}
