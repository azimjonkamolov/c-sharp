// Without using System

namespace Lec3_3
{
    class Program
    {
        static void Main(string[] args)
        {
            // To use withour System

            System.Console.WriteLine("A simple program with C#!");

            System.Console.ReadLine();
        }
    }
}
