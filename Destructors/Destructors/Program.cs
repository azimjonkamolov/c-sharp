// Destructors
using System;

class Destructor
{
    public int x;

    public Destructor(int i)
    {
        x = i;
    }

    // Called when object is recycled
    ~Destructor()
    {
        Console.WriteLine("Destructing " + x);
    }

    // Generates an object that is immediately destroyed.
    public void Generator(int i)
    {
        Destructor o = new Destructor(i);
    }
}


class   DestructDemo
{
    static void Main(string[] args)
    {
        int count;
        Destructor ob = new Destructor(0);
        /* Now, generate a large number of objects. At some point, garbage collection will occur.
           Note: You might need to increase the number of objects generated in order to force
           garbage collection. */

        for (count = 1; count < 100000; count++)

            ob.Generator(count);

        Console.WriteLine("Done!");
        
        Console.ReadKey();

    }
}    
