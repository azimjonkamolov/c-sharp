using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lec4_Squared_Cubed
{
    class Program
    {
        static void Main(string[] args)
        {
            // 
            int iv_1;
            Console.WriteLine("Value\tSquared\t  Cubed");
            for (iv_1 = 1; iv_1 < 100; iv_1++)
                Console.WriteLine("{0}\t {1}\t {2}", iv_1, iv_1 * iv_1, iv_1 * iv_1 * iv_1);

            Console.ReadKey();
        }
    }
}
