using System;

namespace initializing_array
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 10, 20, 30, 40, 50 };
            int i;
            float avg=0;

            for(i=0;i<5;i++)
            {
                avg = avg + arr[i];
            }
            avg = avg / 5;

            Console.WriteLine("The average is " + avg);

            Console.Write("Press any key");
            Console.ReadKey();
        }
    }
}
