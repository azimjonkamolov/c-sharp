using System;

namespace Arrays_of_string
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] sarr = { "This", "is", "a", "test" };
            int i, n=sarr.Length;

            for (i = 0; i < n; i++)
                Console.Write(sarr[i] + " ");

            Console.WriteLine();

            sarr[1] = "was";
            sarr[3] = "test too";

            for (i = 0; i < n; i++)
                Console.Write(sarr[i] + " ");

            Console.WriteLine();

            Console.Write("Press any key...");
            Console.ReadKey();
 
        }
    }
}
