using System;

namespace Jagged_array_one
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] arr = new int[3][];
            int i;

            arr[0] = new int[4];
            arr[1] = new int[3];
            arr[2] = new int[5];

            for (i = 0; i < 4; i++)
                arr[0][i] = i;

            for (i = 0; i < 4; i++)
                Console.Write(arr[0][i] + " ");

            Console.WriteLine();

            for (i = 0; i < 3; i++)
                arr[1][i] = i;

            for (i = 0; i < 3; i++)
                Console.Write(arr[1][i] + " ");

            Console.WriteLine();

            for (i = 0; i < 5; i++)
                arr[2][i] = i;

            for (i = 0; i < 5; i++)
                Console.Write(arr[2][i] + " ");

            Console.WriteLine();

            Console.WriteLine("Press any key...");
            Console.ReadKey();

        }
    }
}
