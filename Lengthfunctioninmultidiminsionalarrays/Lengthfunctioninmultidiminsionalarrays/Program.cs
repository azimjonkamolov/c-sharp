using System;

namespace Lengthfunctioninmultidiminsionalarrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,,] arr = new int[10, 5, 6];
            int n = arr.Length;

            Console.WriteLine("The number of arrays that can be stored in this multidiminsional array is " + n);

            Console.WriteLine();
            Console.WriteLine("Press any key...");
            Console.ReadKey();

        }
    }
}
