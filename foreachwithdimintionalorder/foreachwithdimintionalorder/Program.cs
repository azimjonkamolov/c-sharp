using System;

class Foreachtwo
{
    static void Main()
    {
        int[,] arr = new int[3, 5];
        int i, j, sum = 0, num=1;
        
        for(i=0;i<3;i++)
        {
            for(j=0;j<5;j++)
            {
                arr[i, j] = (i + 1) * (j + 1);
            }
        }

        foreach(int t in arr) // shows row by row
        {
            Console.WriteLine("Value is: " + t);
            sum += t;
        }

        Console.WriteLine("Sum is: " + sum);

        Console.Write("Press any key..");
        Console.ReadKey();
       

    }
}