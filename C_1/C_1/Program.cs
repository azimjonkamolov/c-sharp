using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_1
{
    class Program
    {
        static void Main(string[] args)
        {
            // To deal with variables
            var sv_name = "Tom"; // is ok when data type can be seen here string
            var sv_first = "John";
            var sv_second = "Jack";

            Console.WriteLine(sv_name);
            Console.WriteLine("Hello " + sv_name);
            Console.WriteLine("String interpolation can be seen bellow !");
            Console.WriteLine($"Hello {sv_name}");
            Console.WriteLine($"There are {sv_first} and {sv_second}.");


            Console.ReadLine(); // cannot write Console.Readline("Write here: "); >> x


        }
    }
}
