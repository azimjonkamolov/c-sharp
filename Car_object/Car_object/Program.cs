// How to create two cars with objects
using System;

class Car
{
    public string color;
    public string name;
    public int price;
}



    class Twocars
    {
        static void Main(string[] args)
        {

        Car Tesla = new Car();
        Car BMW = new Car();

        Tesla.color = "red";
        Tesla.name = "Tesla";
        Tesla.price = 100000;

        Console.WriteLine("The first car is " + Tesla.name);
        Console.WriteLine("The first car's color is " + Tesla.color);
        Console.WriteLine("The first car's price is " + Tesla.price);

        Console.WriteLine();

        BMW.color = "blue";
        BMW.name = "BMW";
        BMW.price = 150000;

        Console.WriteLine("The second car is " + BMW.name);
        Console.WriteLine("The second car's color is " + BMW.color);
        Console.WriteLine("The second car's price is " + BMW.price);

        Console.WriteLine();
        Console.ReadKey();

    }
}

