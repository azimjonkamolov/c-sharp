using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetic_Operators
{
    class Program
    {
        static void Main(string[] args)
        {
            // To show simple arithmetic operations
            float iv_1, iv_2, iv_sum;

            Console.WriteLine("Enter value for a: ");
            iv_1=float.Parse(Console.ReadLine());
            Console.WriteLine("Enter value for b: ");
            iv_2 = float.Parse(Console.ReadLine());
            Console.WriteLine("a * b = " + iv_1 * iv_2);
            Console.WriteLine("a / b = " + iv_1 / iv_2);
            iv_sum = iv_1 + iv_2;
            Console.WriteLine("a + b = " + iv_sum);
            iv_sum= iv_1 - iv_2;
            Console.WriteLine("a - b = " + iv_sum);

            Console.ReadKey();

        }
    }
}
