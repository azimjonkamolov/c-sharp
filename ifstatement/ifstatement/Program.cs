using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ifstatement
{
    class Program
    {
        static void Main(string[] args)
        {
            // To use if statement
            int i;
            for (i=-5;i<=5;i++)
            {
                Console.Write("Testing " + i + ": ");

                if (i < 0)
                    Console.WriteLine("negative");
                else
                    Console.WriteLine("positive");
            }

            Console.ReadKey();
        }
    }
}
