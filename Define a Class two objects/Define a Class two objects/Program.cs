// A program that uses the Building class.
using System;

class Building
{
    public int Floors;
    public int Area;
    public int Occupants;
}

// This class declares an object of type Building
class BuildingDemo
{
    static void Main()
    {
        // 

        Building house = new Building(); // to create a Building object
        Building office = new Building(); // to create another Buildin object
        int areaPP; // area per person

        // Assign values to fields in house.
        house.Occupants = 4;
        house.Area = 2500;
        house.Floors = 2;

        // Assign values to fields in office.
        office.Occupants = 25;
        office.Area = 4200;
        office.Floors = 3;

        // to compute the area per person in house
        areaPP = house.Area / house.Occupants;

        Console.WriteLine("House has:\n" +
            house.Floors + " floors\n" +
            house.Occupants + " occupants\n" +
            house.Area + " total area\n" +
            areaPP + " area per person\n");
        Console.WriteLine();

        // to computer the area per person in office
        areaPP = office.Area / office.Occupants;

        Console.WriteLine("Office has:\n" +
            office.Floors + " floors\n" +
            office.Occupants + " occupants\n" +
            office.Area + " total area\n" +
            areaPP + " area per person\n");
        Console.WriteLine();

        Console.ReadKey();
    }
}
