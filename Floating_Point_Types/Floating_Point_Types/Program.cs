using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Floating_Point_Types
{
    class Program
    {
        static void Main(string[] args)
        {
            // To use Floating Point Types with math functions
            Double theta;

            for (theta =0.1; theta<=1.0; theta=theta+0.1)
            {
                Console.WriteLine("Sine of " + theta + " is " + Math.Sin(theta));
                Console.WriteLine("Cosine of " + theta + " is " + Math.Cos(theta));
                Console.WriteLine("Tangent of " + theta + " is " + Math.Tan(theta));

            }

            Console.ReadKey();
        }
    }
}
