using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Illustration_of_for_statement
{
    class Program
    {
        static void Main(string[] args)
        {
            //  for and while loop illustration
            int i, n = 5;

            for (i=1; i<=n; i++)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine("for loop is done!");

            n += 5;

            while(i <= n)
            {
                Console.WriteLine(i);
                i++;
            }

            Console.WriteLine("While loop is done!");

            Console.WriteLine("Thank you!");

            Console.ReadKey();

        }
    }
}
