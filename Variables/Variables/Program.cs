using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Variables
{
    class Program
    {
        static void Main(string[] args)
        {
            // To declare data types
            int v_num = 17;
            string v_name = "Azim";
            decimal v_sallary = 200.10m;
            float v_floatnum = 17.7f;
            double v_doublenum = 15.5d;
            bool v_good;
            v_good = true;
           
            // To gain outputs
            Console.WriteLine("The number is " + v_num);
            Console.WriteLine("The name is " + v_name);
            Console.WriteLine("The sallary is " + v_sallary);
            Console.WriteLine("The float num is " + v_floatnum);
            Console.WriteLine("The double num is " + v_doublenum);
            Console.WriteLine("The good is " + v_good);
            Console.Read();
        }
    }
}
