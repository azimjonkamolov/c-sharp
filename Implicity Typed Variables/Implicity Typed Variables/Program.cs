using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implicity_Typed_Variables
{
    class Program
    {
        static void Main(string[] args)
        {
            // To show some more features
            var s1 = 4.0;
            var s2 = 5.0;

            // now as determined just var is used
            var hypot = Math.Sqrt((s1 * s2) * (s2 * s2));

            Console.Write("Hypotenuse of triangle with sides " + s1 + " by " + s2 + " is ");
            Console.WriteLine("{0:#.###}.", hypot);

            // s1 = 12.2M >> X
            Console.ReadKey();
        }
    }
}
