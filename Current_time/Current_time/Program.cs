using System;
using System.Collections.Generic;
using System.Text;

namespace CurrentDateNTime
{
    public class Time
    {
        // private number variables
        int Year, Month, Date, Hour, Minute, Second;

        //public accessor methods
        public void DisplayCurrentTime()
        {
            System.Console.WriteLine("{0}/{1}/{2}/ {3}:{4}:{5}",
                Month, Date, Year, Hour, Minute, Second);
        }
        
        // constructor
        public Time(System.DateTime dt)
        {
            Year = dt.Year;
            Month = dt.Month;
            Date = dt.Day;
            Hour = dt.Hour;
            Minute = dt.Minute;
            Second = dt.Second;
        }
    }

    public class Tester
    {
        static void Main()
        {
            System.DateTime currentTime = System.DateTime.Now;
            Time t = new Time(currentTime);
            t.DisplayCurrentTime();
            Console.ReadKey();
        }
    }
}
