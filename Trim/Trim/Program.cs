using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trim
{
    class Program
    {
        static void Main(string[] args)
        {
            // To use Trim
            string sv_try = "     You are here       ";
            Console.WriteLine($"[{sv_try}]");

            string sv_trimmed = sv_try.TrimStart();
            Console.WriteLine($"[{sv_trimmed}]");

            sv_trimmed = sv_try.TrimEnd();
            Console.WriteLine($"[{sv_trimmed}]");

            sv_trimmed = sv_try.Trim();
            Console.WriteLine($"[{sv_trimmed}]");

            Console.ReadLine();
        }
    }
}
