using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_type_in_deapth
{
    class Program
    {
        static void Main(string[] args)
        {
            int v_int = 0;
            // int v_x = 2147483648; gives an error
            float v_float;

            // To the max int data type num;
            v_int = int.MaxValue;
            v_float = float.MaxValue;

            Console.WriteLine("The max num int can go up to " + v_int);
            Console.WriteLine("The max num float can go up to " + v_float);

            Console.Read();
        }
    }
}
 