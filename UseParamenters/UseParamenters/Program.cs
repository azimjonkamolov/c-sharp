// A simple example that uses a parameter
using System;

class Chknum
{
    // Return true if x is prime
    public bool IsPrime(int x)
    {
        if (x <= 1)
            return false;

        for (int i = 2; i <= x / i; i++)
            if((x%i)==0) return false;

        return true;
    }
}

class ProgramDemo
{
    static void Main(string[] args)
    {
        Chknum ob = new Chknum();
        int n, i; // n number must be entered
        Console.WriteLine("Enter the n number: ");

        n = int.Parse(Console.ReadLine());

        for(i=2; i<n; i++)
            if (ob.IsPrime(i))
                Console.WriteLine(i + " is prime.");
            else
                Console.WriteLine(i + " is not prime");

        Console.ReadKey();

    }
}
