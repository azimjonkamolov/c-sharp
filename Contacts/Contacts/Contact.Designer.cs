﻿namespace Contacts
{
    partial class gitlabpic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gitlabpic));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.forID = new System.Windows.Forms.Label();
            this.inforID = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.forFirstName = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.forLastName = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.forNo = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.forAddress = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(247, 29);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(294, 55);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // forID
            // 
            this.forID.AutoSize = true;
            this.forID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forID.Location = new System.Drawing.Point(96, 136);
            this.forID.Name = "forID";
            this.forID.Size = new System.Drawing.Size(96, 20);
            this.forID.TabIndex = 1;
            this.forID.Text = "Contact ID";
            this.forID.Click += new System.EventHandler(this.label1_Click);
            // 
            // inforID
            // 
            this.inforID.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.inforID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inforID.Location = new System.Drawing.Point(209, 133);
            this.inforID.Name = "inforID";
            this.inforID.Size = new System.Drawing.Size(199, 26);
            this.inforID.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(209, 177);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(199, 26);
            this.textBox1.TabIndex = 4;
            // 
            // forFirstName
            // 
            this.forFirstName.AutoSize = true;
            this.forFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forFirstName.Location = new System.Drawing.Point(96, 180);
            this.forFirstName.Name = "forFirstName";
            this.forFirstName.Size = new System.Drawing.Size(96, 20);
            this.forFirstName.TabIndex = 3;
            this.forFirstName.Text = "First Name";
            this.forFirstName.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(209, 224);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(199, 26);
            this.textBox2.TabIndex = 6;
            // 
            // forLastName
            // 
            this.forLastName.AutoSize = true;
            this.forLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forLastName.Location = new System.Drawing.Point(96, 227);
            this.forLastName.Name = "forLastName";
            this.forLastName.Size = new System.Drawing.Size(95, 20);
            this.forLastName.TabIndex = 5;
            this.forLastName.Text = "Last Name";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(209, 270);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(199, 26);
            this.textBox3.TabIndex = 8;
            // 
            // forNo
            // 
            this.forNo.AutoSize = true;
            this.forNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forNo.Location = new System.Drawing.Point(96, 273);
            this.forNo.Name = "forNo";
            this.forNo.Size = new System.Drawing.Size(99, 20);
            this.forNo.TabIndex = 7;
            this.forNo.Text = "Contact No";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(209, 314);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(199, 95);
            this.textBox4.TabIndex = 10;
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // forAddress
            // 
            this.forAddress.AutoSize = true;
            this.forAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forAddress.Location = new System.Drawing.Point(96, 317);
            this.forAddress.Name = "forAddress";
            this.forAddress.Size = new System.Drawing.Size(75, 20);
            this.forAddress.TabIndex = 9;
            this.forAddress.Text = "Address";
            // 
            // gitlabpic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 518);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.forAddress);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.forNo);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.forLastName);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.forFirstName);
            this.Controls.Add(this.inforID);
            this.Controls.Add(this.forID);
            this.Controls.Add(this.pictureBox1);
            this.Name = "gitlabpic";
            this.Text = " ";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label forID;
        private System.Windows.Forms.TextBox inforID;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label forFirstName;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label forLastName;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label forNo;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label forAddress;
    }
}

