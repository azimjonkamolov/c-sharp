using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace how_to_print_with_space_in_c_sharp
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 17;
            Console.WriteLine("The number: {0,4}", a);

            Console.Write("Press any key...");
            Console.ReadKey();

        }
    }
}
