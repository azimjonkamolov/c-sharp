using System;

namespace Programtofindtheaverageofgivingnumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[50]; // any number up to 50
            int i, n;
            float avg=0;
            Console.Write("How many numbers you want to enter(up to 50 please): ");
            n = int.Parse(Console.ReadLine());

            for(i=0;i<n;i++)
            {
                arr[i] = int.Parse(Console.ReadLine());
                avg = avg + arr[i];
            }

            avg = avg / n;
            Console.WriteLine("The average of these nums: " + avg);

            Console.Write("Press any key...");
            Console.ReadKey();
        }
    }
}
