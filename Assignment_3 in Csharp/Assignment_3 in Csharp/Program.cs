﻿// Author: Azimjon Kamolov
// Gmail: azimjon.6561@gmail.com
// C# Assignment 3 task 1

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_3_in_Csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            // To convert USA dollar to Korean won as of 4/2/2019
            double US, WON;
            int i = 0, n = 5;
            Console.WriteLine("Note: Convertion from USA dollar to Korean won goes as of 4/2/2019\n");
            for (i=0;i<5;i++)
            {
                
                Console.Write("Enter your money in US dollar: ");
                US = double.Parse(Console.ReadLine());
                WON = US * 1135.44;
                Console.WriteLine($"{US} USD is {WON} KOW");
                Console.WriteLine();
            }
            
            Console.ReadKey();

            //
        }
    }
}
