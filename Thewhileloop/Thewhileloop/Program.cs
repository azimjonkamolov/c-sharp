using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thewhileloop
{
    class Program
    {
        static void Main(string[] args)
        {
            // To computer the order of magnitude of an integer
            int num = 4356799;
            int mag = 0;

            Console.WriteLine("Number: " + num);

            while(num>0)
            {
                mag++;
                num = num / 10;
            }

            Console.WriteLine("Magnitude: " + mag);
            Console.ReadKey();
        }
    }
}
