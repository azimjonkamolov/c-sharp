// Use new with a value type
using System;

class newValue
{
    static void Main()
    {
        // initialize i to zero
        int i = new int(); // now i is zero

        Console.WriteLine("The value of i is: " + i);

        Console.ReadKey();
    }
}