using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integers
{
    class Program
    {
        static void Main(string[] args)
        {
            // To use integer data type
            long inches; // as a huge number is going to be stored in this varables long is declared
            long miles;  // as a huge number is going to be stored in this varables long is declared

            miles = 93000000;
            // 93,000,000 miles to the sun
            // 5,280 feet in a mile
            // 12 inches in a foot
            inches = miles * 5280 * 12; //To calculate the dictance to the sun from the earth in inches

            Console.WriteLine("Distance to the sun: " + inches + " inches.");

            Console.ReadKey();
        }
    }
}
