// A simple constructor
using System;

class myClass
{
    public int x;

    public myClass()
    {
        x = 10;
    }
}

class ConsDemo
{
    static void Main()
    {
        myClass t1 = new myClass();
        myClass t2 = new myClass();

        Console.WriteLine(t1.x + " " + t2.x);

        Console.ReadKey();
    }
}