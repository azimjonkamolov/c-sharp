using System;

namespace Assigning_array_references
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[10];
            int[] arr1 = new int[10];
            int i;

            for (i = 0; i < 10; i++)
                arr[i] = i;

            for (i = 0; i < 10; i++)
                Console.Write(arr[i] + " ");

            Console.WriteLine();

            for (i = 0; i < 10; i++)
                arr1[i] = -i;

            for (i = 0; i < 10; i++)
                Console.Write(arr1[i] + " ");

            arr1 = arr;
            Console.WriteLine();

            Console.Write("After arr1 = arr : ");

            for (i = 0; i < 10; i++)
                Console.Write(arr1[i] + " ");

            Console.WriteLine();

            arr[4] = 99;

            Console.Write("After arr[4]=0: ");
            for (i = 0; i < 10; i++)
                Console.Write(arr[i] + " ");


            Console.WriteLine();
            Console.Write("Press any key..");
            Console.ReadKey();

        }
    }
}
